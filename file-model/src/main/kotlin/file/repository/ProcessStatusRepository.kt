package file.repository

import file.model.ProcessStatus
import java.sql.ResultSet
import java.sql.Timestamp
import java.time.OffsetDateTime
import java.util.*
import javax.inject.Singleton
import javax.sql.DataSource

@Singleton
class ProcessStatusRepository(private val dataSource: DataSource) : JdbcRepository<ProcessStatus>(dataSource = dataSource) {

    fun findByRequestId(requestId: UUID): Optional<ProcessStatus>
        = findOne("select * from file.process_status where request_id = ?", rowMapper, listOf(requestId))

    fun findAllByClientId(clientId: UUID): List<ProcessStatus>
        = findAll("select * from file.process_status where client_id = ? and (status in ('Uploaded', 'Processing') or created_timestamp > now() - interval '5 minute') order by created_timestamp", rowMapper, listOf(clientId))

    fun findByIds(ids: Iterable<UUID>): List<ProcessStatus>
            = findAll("select * from file.process_status where request_id in (${placeholders(ids.count())}) order by created_timestamp", rowMapper, ids)

    fun insert(processStatus: ProcessStatus)
        = update("insert into file.process_status (request_id, client_id, status, filename, total_lines, lines_processed, created_timestamp) values (?, ?, ?::file.status, ?, ?, ?, ?)",
            listOf(processStatus.requestId, processStatus.clientId, processStatus.status.toString(), processStatus.filename, processStatus.totalLines, 0, Timestamp(processStatus.createdTimestamp.toInstant().toEpochMilli()))
        )

    fun update(processStatus: ProcessStatus)
        = update("update file.process_status set status=?::file.status, lines_processed=?, updated_timestamp=? where request_id=?",
            listOf(processStatus.status.toString(), processStatus.linesProcessed,
                    toTimestamp(processStatus.updatedTimestamp), processStatus.requestId)
        )

    private fun toTimestamp(value: OffsetDateTime?):Timestamp?
        = if (value == null) null else Timestamp(value.toInstant().toEpochMilli())

    private val rowMapper = { rs: ResultSet, _: Int ->
        ProcessStatus(
                requestId = rs.getObject("request_id") as UUID,
                clientId = rs.getObject("client_id") as UUID,
                status = ProcessStatus.Status.valueOf(rs.getString("status")),
                filename = rs.getString("filename"),
                totalLines = rs.getLong("total_lines"),
                linesProcessed = rs.getLong("lines_processed"),
                createdTimestamp = rs.getTimestamp("created_timestamp").toLocalDateTime().atOffset(OffsetDateTime.now().offset),
                updatedTimestamp = rs.getTimestamp("updated_timestamp")?.toLocalDateTime()?.atOffset(OffsetDateTime.now().offset)
        )
    }

}