package file.repository

import java.sql.PreparedStatement
import java.sql.ResultSet
import java.util.*
import javax.sql.DataSource

abstract class JdbcRepository<E>(private val dataSource: DataSource) {

    private fun bindParameters(stmt: PreparedStatement, parameters: Iterable<Any?>? = null) {
        parameters?.withIndex()?.forEach { pair -> stmt.setObject(pair.index+1, pair.value) }
    }

    protected fun findAll(sql: String, rowMapper: (ResultSet, Int) -> E, parameters: Iterable<Any?>?= null): List<E> {
        dataSource.connection.use { conn ->
            conn.prepareStatement(sql).use { stmt ->
                bindParameters(stmt, parameters)

                stmt.executeQuery().use { rs ->
                    val result: MutableList<E> = mutableListOf()
                    var index = 0
                    while (rs.next()) {
                        index++
                        result.add(rowMapper(rs, index))
                    }
                    return result.toList()
                }
            }
        }
    }

    protected fun findOne(sql: String, rowMapper: (ResultSet, Int) -> E, parameters: Iterable<Any>? = null): Optional<E> {
        val result = findAll(sql, rowMapper, parameters)
        return Optional.ofNullable(result.getOrNull(0))
    }

    protected fun update(sql: String, parameters: Iterable<Any?>? = null): Int {
        return dataSource.connection.use { conn ->
            conn.prepareStatement(sql).use { stmt ->
                bindParameters(stmt, parameters)

                stmt.executeUpdate()
            }
        }
    }

    protected fun placeholders(n: Int): String = "?" + (",?".repeat(n - 1))

}