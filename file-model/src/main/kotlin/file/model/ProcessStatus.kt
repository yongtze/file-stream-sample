package file.model

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.OffsetDateTime
import java.util.*

data class ProcessStatus(
        val requestId: UUID = UUID.randomUUID(),
        val clientId: UUID? = null,
        val status: Status = ProcessStatus.Status.Uploaded,
        val filename: String? = null,
        val totalLines: Long,
        val linesProcessed: Long? = null,
        val createdTimestamp: OffsetDateTime = OffsetDateTime.now(),
        val updatedTimestamp: OffsetDateTime? = null
) {
    enum class Status {
        @JsonProperty("Uploaded")
        Uploaded,
        @JsonProperty("Processing")
        Processing,
        @JsonProperty("Done")
        Done,
        @JsonProperty("Error")
        Error
    }
}