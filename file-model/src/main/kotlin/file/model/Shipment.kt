package file.model

import java.math.BigDecimal
import java.time.LocalDateTime
import java.util.*

data class Shipment(
    val id: UUID = UUID.randomUUID(),
    val shipperName: String,
    val shipperId: Long? = null,
    val carrierName: String,
    val carrierId: Long? = null,
    val serviceDescription: String,
    val serviceCode: String? = null,
    val weight: BigDecimal?,
    val shipDate: LocalDateTime?,
    val deliveryDate: LocalDateTime?,

    val shipFromName: String?,
    val shipFromAddress1: String?,
    val shipFromAddress2: String?,
    val shipFromCity: String?,
    val shipFromState: String?,
    val shipFromZip: String?,
    val shipFromCountry: String?,

    val shipToName: String?,
    val shipToAddress1: String?,
    val shipToAddress2: String?,
    val shipToCity: String?,
    val shipToState: String?,
    val shipToZip: String?,
    val shipToCountry: String?
)