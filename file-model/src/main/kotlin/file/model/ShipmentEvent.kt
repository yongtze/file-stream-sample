package file.model

import java.time.OffsetDateTime
import java.util.*

data class ShipmentEvent(
        val eventId: UUID = UUID.randomUUID(),
        val requestId: UUID,
        val timestamp: OffsetDateTime = OffsetDateTime.now(),
        val totalLines: Long?,
        val data: Shipment
)