create schema if not exists file;

set search_path = file;

create type status as enum ('Uploaded', 'Processing', 'Done');

create table file.process_status (
    request_id uuid primary key,
    client_id uuid not null,
    status status,
    filename text,
    total_lines int default 0,
    lines_processed int default 0,
    created_timestamp timestamp with time zone default now(),
    updated_timestamp timestamp with time zone
);

create table file.roles (
    role_name text primary key,
    role_description text not null
);

create table file.organizations (
    id serial primary key,
    name text not null,
    role text not null references file.roles(role_name)
);

create table file.services (
    service_id serial primary key,
    carrier_id integer not null references file.organizations(id),
    service_code text,
    service_description text,
    constraint services_code_unq unique(carrier_id, service_code)
);

create table file.countries (
    country_code text primary key,
    country_name text not null
);

insert into file.roles(role_name, role_description) values ('Shipper', 'An organization who procures shipping services.');
insert into file.roles(role_name, role_description) values ('Carrier', 'An organization who provides shipping services.');
insert into file.roles(role_name, role_description) values ('ThirdParty', 'An organization who provide value-add services to the shipping process, e.g. freight forwarder, freight auditor, etc.');

insert into file.organizations(name, role) values ('ACME', 'Shipper');
insert into file.organizations(name, role) values ('UPS', 'Carrier');

insert into file.countries (country_code, country_name) values ('US', 'United States');
