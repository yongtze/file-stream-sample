package file.aggregator.kafka

import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import file.repository.ProcessStatusRepository
import file.model.ProcessStatus
import file.model.ShipmentEvent
import io.micronaut.configuration.kafka.serde.JsonSerde
import io.micronaut.configuration.kafka.streams.ConfiguredStreamBuilder
import io.micronaut.context.annotation.Factory
import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.streams.StreamsConfig
import org.apache.kafka.streams.kstream.*
import java.time.OffsetDateTime
import java.util.*
import javax.inject.Named
import javax.inject.Singleton

@Factory
class ProcessStatusAggregator {

    @Singleton
    @Named("process-status-aggregator")
    fun aggregator(processStatusRepository: ProcessStatusRepository, builder: ConfiguredStreamBuilder): KStream<UUID, ShipmentEvent> {
        val jsonMapper = jacksonObjectMapper()
        jsonMapper.registerModules(JavaTimeModule(), KotlinModule())
        jsonMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)

        // Set streams aggregation commit interval to 5 secs.
        builder.configuration[StreamsConfig.COMMIT_INTERVAL_MS_CONFIG] = 1500

        // Read from enriched-shipments topic.
        val stream = builder.stream("enriched-shipments",
                Consumed.with(Serdes.UUID(), JsonSerde(jsonMapper, ShipmentEvent::class.java)))

        // Transformed into ProcessStatus and group by requstId
        val groupedStream = stream
            .mapValues { event: ShipmentEvent ->
                ProcessStatus(requestId = event.requestId, totalLines = event.totalLines!!, linesProcessed = 1)
            }
            .groupBy(
                { _, ps: ProcessStatus -> ps.requestId },
                Grouped.with(Serdes.UUID(), JsonSerde(jsonMapper, ProcessStatus::class.java))
            )

        // Aggregate ProcessStatus records to sum up linesProcessed.
        val table = groupedStream
            .aggregate(
                    { ProcessStatus(linesProcessed = 0, totalLines = 0) },
                    { id: UUID, value: ProcessStatus, aggValue: ProcessStatus ->
                        val linesProcessed = (value.linesProcessed!! + aggValue.linesProcessed!!)
                        val status = status(value.totalLines, linesProcessed)
                        val timestamp = OffsetDateTime.now()
                        ProcessStatus(
                                requestId = id,
                                totalLines = value.totalLines,
                                linesProcessed = linesProcessed,
                                status = status,
                                createdTimestamp = timestamp,
                                updatedTimestamp = timestamp
                        )
                    },
                    Materialized.with(Serdes.UUID(), JsonSerde(jsonMapper, ProcessStatus::class.java))
            )

        // Convert aggregation results into another stream, then write to output topic and database.
        table
            .toStream()
            .through("process-status-updates", Produced.with(Serdes.UUID(), JsonSerde(jsonMapper, ProcessStatus::class.java)))
            .foreach { _, processStatus -> processStatusRepository.update(processStatus) }

        return stream
    }

    private fun status(totalLines: Long, linesProcessed: Long): ProcessStatus.Status
        = if (linesProcessed < totalLines) ProcessStatus.Status.Processing else ProcessStatus.Status.Done

}