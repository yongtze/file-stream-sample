package file.aggregator

import io.micronaut.runtime.Micronaut

object FileAggregatorApp {

    @JvmStatic
    fun main(args: Array<String>) {
        Micronaut.build()
                .packages("file.aggregator")
                .mainClass(FileAggregatorApp.javaClass)
                .start()
    }
}