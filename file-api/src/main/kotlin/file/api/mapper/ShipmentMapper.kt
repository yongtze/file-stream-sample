package file.api.mapper

import file.model.Shipment
import java.math.BigDecimal
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

val dateFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")

fun parseDate(value: String?): LocalDateTime?
    = if (value.isNullOrEmpty()) null else LocalDate.parse(value, dateFormatter).atTime(0, 0)

fun tupleToShipment(tuple: List<String>): Shipment =
        Shipment(
                shipperName = tuple[0],
                carrierName = tuple[1],
                serviceDescription = tuple[2],
                weight = BigDecimal(tuple[3]),
                shipDate = parseDate(tuple[4]),
                deliveryDate = parseDate(tuple[5]),
                shipFromName = tuple[6],
                shipFromAddress1 = tuple[7],
                shipFromAddress2 = tuple[8],
                shipFromCity = tuple[9],
                shipFromState = tuple[10],
                shipFromZip = tuple[11],
                shipFromCountry = tuple[12],
                shipToName = tuple[13],
                shipToAddress1 = tuple[14],
                shipToAddress2 = tuple[15],
                shipToCity = tuple[16],
                shipToState = tuple[17],
                shipToZip = tuple[18],
                shipToCountry = tuple[19]
        )