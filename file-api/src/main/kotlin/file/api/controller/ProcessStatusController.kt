package file.api.controller

import file.model.ProcessStatus
import file.repository.ProcessStatusRepository
import io.micronaut.http.annotation.*
import java.util.*


@Controller("/api/processStatus")
class ProcessStatusController(private val processStatusRepository: ProcessStatusRepository) {

    @Get("/")
    fun findAllByClientId(@Header("X-Client-ID") clientId: UUID): List<ProcessStatus>
        = processStatusRepository.findAllByClientId(clientId)

}