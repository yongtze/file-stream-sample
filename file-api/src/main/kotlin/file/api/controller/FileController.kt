package file.api.controller

import file.api.kafka.ShipmentRequestProducer
import file.api.mapper.tupleToShipment
import file.model.ShipmentEvent
import file.model.ProcessStatus
import file.repository.ProcessStatusRepository
import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Header
import io.micronaut.http.annotation.Post
import io.micronaut.http.multipart.CompletedFileUpload
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.StandardCopyOption
import java.util.*

@Controller("/api/file")
class FileController(
        private val shipmentRequestProducer: ShipmentRequestProducer,
        private val processStatusRepository: ProcessStatusRepository) {

    companion object FileController {
        val log:Logger = LoggerFactory.getLogger(FileController::class.java)
    }

    @Post(value = "/upload", consumes = [ MediaType.MULTIPART_FORM_DATA ])
    fun upload(@Header("X-Client-ID") clientId: UUID, file: CompletedFileUpload): HttpResponse<ProcessStatus> {
        val requestId = UUID.randomUUID()
        try {
            val tempFile = prepareTempFile(file)
            val totalLines = lineCount(tempFile) - 1
            val ps = ProcessStatus(
                    requestId = requestId,
                    clientId = clientId,
                    status = ProcessStatus.Status.Uploaded,
                    filename = file.filename,
                    totalLines = totalLines,
                    linesProcessed = 0)
            processStatusRepository.insert(ps)

            sendToKafka(tempFile, ps)

            return HttpResponse.ok(ps)
        } catch (e: Exception) {
            log.error(e.message, e)
            return HttpResponse.serverError(
                    ProcessStatus(
                            requestId = requestId,
                            clientId = clientId,
                            status = ProcessStatus.Status.Error,
                            filename = file.filename,
                            totalLines = 0
                    )
            )
        }
    }

    private fun prepareTempFile(file: CompletedFileUpload): Path {
        val tempFile = Files.createTempFile("upload-", ".tmp")
        val tempPath = tempFile.toAbsolutePath()
        Files.copy(file.inputStream, tempPath, StandardCopyOption.REPLACE_EXISTING)
        return tempPath
    }

    private fun lineCount(file: Path): Long
            = Files.lines(file).count()

    private fun sendToKafka(file: Path, processStatus: ProcessStatus) {
        try {
            Files.lines(file)
                    .skip(1)    // Skip the header line
                    .forEach { line ->
                        val vals = line.split("\t")
                        val shipment = tupleToShipment(vals)
                        val event = ShipmentEvent(
                                requestId = processStatus.requestId,
                                totalLines = processStatus.totalLines,
                                data = shipment)
                        shipmentRequestProducer.send(event.eventId, event)
                    }
        } finally {
            Files.deleteIfExists(file)
        }
    }

}


