package file.api

import io.micronaut.runtime.Micronaut

object FileAPIApp {

    @JvmStatic
    fun main(args: Array<String>) {
        Micronaut.build()
                .packages("file.api")
                .mainClass(FileAPIApp.javaClass)
                .start()
    }
}