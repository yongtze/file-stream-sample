package file.api.kafka

import file.model.ShipmentEvent
import io.micronaut.configuration.kafka.annotation.KafkaClient
import io.micronaut.configuration.kafka.annotation.KafkaKey
import io.micronaut.configuration.kafka.annotation.Topic
import java.util.*

@KafkaClient
interface ShipmentRequestProducer {

    @Topic("shipment-requests")
    fun send(@KafkaKey eventId: UUID, event: ShipmentEvent)

}