import React from 'react';
import './App.css';
import UploadZone from './components/UploadZone';
import ProgressPanel from './components/ProgressPanel';
import uuidv4 from 'uuid/v4';


export default class App extends React.Component {

  constructor(props) {
    super(props);
    this.apiBaseUrl = "http://localhost:9000";
    this.clientId = uuidv4();
    this.state = {
      processStatuses: [ ]
    };
  }

  render() {
    return (
      <div className="App">
        <div className="App-container">
          <div className="row App-header">
            <div className="col"><h1>Upload Your Files</h1></div>
          </div>

          <div className="row App-body">
            <div className="col">
              <UploadZone uploadHandler={this.uploadFiles}></UploadZone>
            </div>
            <div className="col">
              <ProgressPanel processStatuses={this.state.processStatuses}></ProgressPanel>
            </div>
          </div>
          
        </div>
      </div>
    );
  }

  uploadFiles = (files) => {
    var formData = new FormData();

    formData.append("file", files[0]);

    fetch(this.apiBaseUrl + '/api/file/upload', {
        method: 'POST',
        headers: { 'X-Client-ID': this.clientId },
        body: formData,
      })
      .then(response => response.json())
      .then(success => this.refresh())
      .catch(error => console.log(error));
  }

  refresh = () => {
    fetch(this.apiBaseUrl + "/api/processStatus/", {
        headers: { 'X-Client-ID': this.clientId }
      })
      .then(response => response.json())
      .then(processStatuses => {
        this.setState({ processStatuses: processStatuses });
        if (this.needAutoRefresh(processStatuses)) {
          setTimeout(this.refresh, 1000);
        }
      });
  }

  needAutoRefresh = (processStatuses) => {
    if (processStatuses === null) {
      return false;
    }
    let count = processStatuses
      .filter(ps => ps.status === "Uploaded" || ps.status === "Processing")
      .map(ps => 1)
      .reduce(((acc, val) => acc + val), 0);
    return count > 0;
  }

}
