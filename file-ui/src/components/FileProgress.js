import React from 'react';
import './FileProgress.css';

export default function FileProgress(props) {
  let status = props.processStatus;
  let pcnt = round(status.linesProcessed * 100.0 / status.totalLines, 1);
  let progressText
  if (pcnt >= 100) {
    progressText = "DONE";
  } else {
    progressText = "" + pcnt + "% Processed";
  }

  return (
    <div className="list-group-item">
      <div className="filename">{status.filename}</div>
      <div className="progress">
        <div className="progress-bar" role="progressbar" style={{ width: pcnt + '%' }}></div>
      </div>
      <div className="progress-text">{progressText}</div>
    </div>
  );
}

function round(value, decimals) {
  return Number(Math.round(value+'e'+decimals)+'e-'+decimals);
}
