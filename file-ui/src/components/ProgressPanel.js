import React from 'react';
import FileProgress from './FileProgress';

export default class ProgressPanel extends React.Component {

  render() {
    let processStatuses = this.props.processStatuses;
    let listItems = processStatuses.map((ps) =>
      <FileProgress processStatus={ps} key={ps.requestId}></FileProgress>
    )
    return (
      <div className="list-group progress-list">{listItems}</div>
    );
  }
}