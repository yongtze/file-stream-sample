import React from 'react';
import './UploadZone.css';
import Dropzone from 'react-dropzone';
import PropTypes from 'prop-types';

export default class UploadZone extends React.Component {
  static propTypes = {
    uploadHandler: PropTypes.func,
  };

  render() {
    return (
      <Dropzone onDrop={this.props.uploadHandler}
          method="post"
          accept="text/plain, text/tab-separated-values">
        {({getRootProps, getInputProps}) => 
          <div className="dropzone" {...getRootProps()}>
            <input {...getInputProps()} />
            <h4>Drag and Drop files here</h4>
            <div className="space-top">OR</div>
            <div className="space-top"><button type="button" className="btn btn-primary select-files">Select Files</button></div>
          </div>
        }
      </Dropzone>
    );
  }
}
