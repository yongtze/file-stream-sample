
# Streaming File Processing

This is a sample application demonstrating processing files through a stream processing pipeline. See the full article
describing how it works [here](https://medium.com/@yongtze_chi/processing-files-in-a-stream-processing-pipeline-2cc6e70e3439).

Follow instructions below to run the application stack on your local machine.

## Prerequisites

1. Docker
2. Node.js
3. JDK 8+

### Docker

In order to run this sample, we'll need to install Docker. If you are running MacOS or Windows, install Docker Desktop
from here: https://www.docker.com/products/docker-desktop. If you are running Linux, most likely, Docker is already
pre-installed. If not, you can easily download from your Linux distribution package repository.

### Node.js

Follow the installation instructions here: https://nodejs.org/en/download/

## How to Run

### Docker Compose

1. Open the Terminal or Command Prompt app.
2. Change working directory to the project folder.
3. Start the docker environment:

```shell script
docker-compose up -d
```

This should start the PostgreSQL and Kafka environment on your local machine.

### Initialize the Database Schema

Run the database migration scripts like this (Linux or MacOS):

```shell script
./gradlew :file-model:flywayMigrate
```

For Windows users:

```shell script
.\gradlew.bat :file-model:flywayMigrate
```

### Create Kafka Topics

To create the Kafka topics required by the App, run the following command:

```shell script
docker-compose exec kafka-broker kafka-topics \
    --bootstrap-server localhost:9092 \
    --create --topic shipment-requests \
    --partitions 1 --replication-factor 1
```
```shell script
docker-compose exec kafka-broker kafka-topics \
    --bootstrap-server localhost:9092 \
    --create --topic enriched-shipments \
    --partitions 1 --replication-factor 1
```
```shell script
docker-compose exec kafka-broker kafka-topics \
    --bootstrap-server localhost:9092 \
    --create --topic process-status-updates \
    --partitions 1 --replication-factor 1
```

### Start the Backend Apps

The backend apps can be built and started using Gradle. You may need to open multiple Terminal or Command Prompt
windows to run all of them.

```shell script
./gradlew :file-api:run
```
```shell script
./gradlew :file-worker:run
```
```shell script
./gradlew :file-aggregator:run
```

### Start the UI server

The UI is served using the npm tool:

```shell script
cd file-ui
npm start
```

Navigate your browser to http://localhost:3000/

At this point, you should have a fully functional environment running on your local machine.

