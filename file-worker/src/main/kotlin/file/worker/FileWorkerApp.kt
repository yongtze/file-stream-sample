package file.worker

import io.micronaut.runtime.Micronaut

object FileWorkerApp {

    @JvmStatic
    fun main(args: Array<String>) {
        Micronaut.build()
                .packages("file.worker")
                .mainClass(FileWorkerApp.javaClass)
                .start()
    }

}