package file.worker.model

data class Service(val serviceCode: String, val serviceDescription: String)