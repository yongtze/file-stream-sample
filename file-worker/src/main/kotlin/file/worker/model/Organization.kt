package file.worker.model

data class Organization(val id: Long, val name: String, val role: Role) {
    enum class Role {
        Shipper, Carrier, ThirdParty
    }
}