package file.worker.model

data class Country(val countryCode: String, val countryName: String)