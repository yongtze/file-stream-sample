package file.worker.kafka

import file.model.ShipmentEvent
import io.micronaut.configuration.kafka.annotation.KafkaClient
import io.micronaut.configuration.kafka.annotation.KafkaKey
import io.micronaut.configuration.kafka.annotation.Topic
import java.util.*

@KafkaClient
interface EnrichedShipmentProducer {

    @Topic("enriched-shipments")
    fun send(@KafkaKey eventId: UUID, event: ShipmentEvent)

}