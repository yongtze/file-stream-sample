package file.worker.kafka

import io.micronaut.configuration.kafka.annotation.KafkaListener
import io.micronaut.configuration.kafka.annotation.Topic

import file.model.*
import file.worker.model.Organization
import file.worker.repository.CountryRepository
import file.worker.repository.OrganizationRepository
import file.worker.repository.ServiceRepository
import io.micronaut.messaging.Acknowledgement
import io.micronaut.messaging.annotation.Body

@KafkaListener(groupId = "shipment-enrichment-worker")
class ShipmentEnrichmentWorker(
        private val countryRepository: CountryRepository,
        private val serviceRepository: ServiceRepository,
        private val organizationRepository: OrganizationRepository,
        private val enrichedShipmentProducer: EnrichedShipmentProducer
) {

    @Topic("shipment-requests")
    fun enrich(@Body event: ShipmentEvent, acknowledgement: Acknowledgement) {

        // Normalize/Enrich shipment data
        val shipment = event.data
        val enriched = shipment.copy(
                shipperId = findOrganizationId(shipment.shipperName, Organization.Role.Shipper),
                carrierId = findOrganizationId(shipment.carrierName, Organization.Role.Carrier),
                serviceCode = findServiceCode(shipment.serviceDescription),
                shipFromCountry = cleanCountry(shipment.shipFromCountry),
                shipToCountry = cleanCountry(shipment.shipToCountry)
        )
        val event2 = event.copy(data = enriched)

        // Artificial delay to make effect of progress updates on UI more prominent.
        Thread.sleep(500);

        // Send the event and acknowledge original event.
        enrichedShipmentProducer.send(event2.eventId, event2)
        acknowledgement.ack()
    }

    private fun findOrganizationId(organizationName: String, role: Organization.Role): Long?
        = organizationRepository.findByNameAndRole(organizationName, role).map{ it.id }.orElse(null)

    private fun findServiceCode(serviceDescription: String): String?
        = serviceRepository.findByServiceDescription(serviceDescription).map { it.serviceCode }.orElse(null)

    private fun cleanCountry(country: String?): String
        = if (country.isNullOrEmpty()) ""
          else countryRepository.findByCountryName(country).map { it.countryCode }.orElse(country)

}