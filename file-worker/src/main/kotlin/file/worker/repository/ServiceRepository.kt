package file.worker.repository

import file.repository.JdbcRepository
import file.worker.model.Service
import java.sql.ResultSet
import java.util.*
import javax.inject.Singleton
import javax.sql.DataSource

@Singleton
class ServiceRepository(private val dataSource: DataSource) : JdbcRepository<Service>(dataSource = dataSource) {

    fun findByServiceDescription(serviceDescription: String): Optional<Service>
        = findOne("select * from file.services where service_description = ?", rowMapper, listOf(serviceDescription.trim()))

    private val rowMapper = { rs: ResultSet, _: Int ->
        Service(serviceCode = rs.getString(1), serviceDescription = rs.getString(2))
    }
}