package file.worker.repository

import file.repository.JdbcRepository
import file.worker.model.Organization
import java.sql.ResultSet
import java.util.*
import javax.inject.Singleton
import javax.sql.DataSource

@Singleton
class OrganizationRepository(private val dataSource: DataSource) : JdbcRepository<Organization>(dataSource = dataSource) {

    fun findByNameAndRole(name: String, role: Organization.Role): Optional<Organization>
            = findOne("select * from file.organizations where name = ? and role = ?", rowMapper, listOf(name.trim(), role.toString()))

    private val rowMapper = { rs: ResultSet, _:Int ->
        Organization(
                id = rs.getLong(1),
                name = rs.getString(2),
                role = Organization.Role.valueOf(rs.getString(3))
        )
    }

}