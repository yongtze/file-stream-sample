package file.worker.repository

import file.repository.JdbcRepository
import file.worker.model.Country
import java.sql.ResultSet
import java.util.*
import javax.inject.Singleton
import javax.sql.DataSource

@Singleton
class CountryRepository(private val dataSource: DataSource) : JdbcRepository<Country>(dataSource = dataSource) {

    fun findByCountryName(countryName: String): Optional<Country>
        = findOne("select * from file.countries where country_name = ?", rowMapper, listOf(countryName.trim()))

    private val rowMapper = { rs: ResultSet, _: Int ->
        Country(countryCode = rs.getString(1), countryName = rs.getString(2))
    }

}